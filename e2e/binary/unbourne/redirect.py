from __future__ import annotations

from pathlib import Path
from subprocess import run

from binary import Diff, Relative, Tool


def test_redirect(tool: Tool, relative: Relative, tmp_path: Path) -> None:
    binary = tool("unbourne")
    expected = relative("expected.txt")
    output = tmp_path / "output.txt"

    stdin = f"echo hello > {str(output)}".encode()
    cmd = (binary,)
    res = run(cmd, check=True, timeout=30, input=stdin,)

    assert Diff(expected) == Diff(output)
