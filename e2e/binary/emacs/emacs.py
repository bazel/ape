from __future__ import annotations

from pathlib import Path
from subprocess import run, PIPE

from binary import Diff, Relative, Tool


def test_emacs(tool: Tool, relative: Relative, tmp_path: Path) -> None:
    binary = tool("emacs")

    cmd = (binary, "--batch", "--eval=(prin1 \"Hello, world!\")")
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)
    assert "\"Hello, world!\"" == result.stdout
