from __future__ import annotations

from subprocess import run

from binary.tool import Tool
from binary import Relative


def test_jq(tool: Tool, relative: Relative) -> None:
    binary = tool("jq")
    fixture = relative("fixture.json")

    cmd = (binary, ".one", fixture)
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)
    assert "1\n" == result.stdout
