from __future__ import annotations

from subprocess import run

from binary.tool import Tool
from binary import Relative


def test_join(tool: Tool, relative: Relative) -> None:
    binary = tool("join")
    file1 = relative("file1.txt")
    file2 = relative("file2.txt")

    cmd = (binary, file1, file2)
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)
    assert """1 One Dogs
2 Two Hippos
3 Three Cats
4 Four Birds
5 Five Mice
""" == result.stdout
