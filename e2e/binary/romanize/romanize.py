from __future__ import annotations

from subprocess import run

from binary.tool import Tool


def test_romanize(tool: Tool) -> None:
    binary = tool("romanize")

    cmd = (binary)
    stdin = 'Mjød'
    result = run(cmd, check=True, timeout=30, input=stdin, capture_output=True, text=True)

    assert result.stdout == 'MJOED'
