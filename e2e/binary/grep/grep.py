from __future__ import annotations

from subprocess import run

from binary.tool import Tool
from binary import Relative


def test_grep(tool: Tool, relative: Relative) -> None:
    binary = tool("grep")
    fixture = relative("fixture.txt")

    cmd = (binary, "lorem", fixture, "-c")
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)
    assert "6\n" == result.stdout
