from __future__ import annotations

from pathlib import Path
from subprocess import run

from binary import Diff, Relative, Tool


def test_csplit(tool: Tool, relative: Relative, tmp_path: Path) -> None:
    binary = tool("csplit")
    fixture = relative("fixture.txt").absolute()
    expected_00 = relative("expected_00.txt")
    expected_01 = relative("expected_01.txt")
    output_00 = tmp_path / "xx00"
    output_01 = tmp_path / "xx01"

    cmd = (binary, fixture, "4")
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True, cwd=tmp_path)
    assert "973\n5648\n" == result.stdout
    assert Diff(expected_00) == Diff(output_00)
    assert Diff(expected_01) == Diff(output_01)
