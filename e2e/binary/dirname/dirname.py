from __future__ import annotations

from subprocess import run

from binary.tool import Tool


def test_dirname(tool: Tool) -> None:
    binary = tool("dirname")

    cmd = (binary, "/my/great/path/file")
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)
    assert "/my/great/path\n" == result.stdout
