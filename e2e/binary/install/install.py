from __future__ import annotations

from pathlib import Path
from subprocess import run

from binary import Diff, Relative, Tool


def test_install(tool: Tool, relative: Relative, tmp_path: Path) -> None:
    binary = tool("install")
    fixture = relative("fixture.txt")
    output = tmp_path / "output.txt"

    cmd = (binary, fixture, output,)
    run(cmd, check=True, timeout=30)

    assert Diff(fixture) == Diff(output)
