from __future__ import annotations

from subprocess import run, CalledProcessError
import pytest

from binary.tool import Tool


def test_strings_equal(tool: Tool) -> None:
    binary = tool("test")
    
    cmd = (binary, 'hello', '=', 'hello')
    run(cmd, check=True, timeout=30)
    

def test_strings_unequal(tool: Tool) -> None:
    binary = tool("test")
    
    cmd = (binary, 'hello', '=', 'bye')
    with pytest.raises(CalledProcessError):
        run(cmd, check=True, timeout=30)
