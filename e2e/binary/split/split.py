from __future__ import annotations

from subprocess import run
from pathlib import Path

from binary import Diff, Relative, Tool


def test_split(tool: Tool, relative: Relative, tmp_path: Path) -> None:
    binary = tool("split")
    fixture = Path.cwd() / relative("fixture.txt")
    output_map = {
        tmp_path / "xaa": relative("xaa.expected.txt"),
        tmp_path / "xab": relative("xab.expected.txt"),
    }

    cmd = (binary, '-l', '2', fixture,)
    run(cmd, check=True, timeout=30, cwd=tmp_path,)

    for output, expected in output_map.items():
        assert Diff(expected) == Diff(output)
