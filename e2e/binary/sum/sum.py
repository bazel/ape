from __future__ import annotations

from subprocess import run
from pathlib import Path

from binary.tool import Tool


def test_sum(tool: Tool, tmp_path: Path) -> None:
    binary = tool("sum")
    input_path = tmp_path / "input_file"

    with open(input_path, "w") as file:
        file.write("hello")
    cmd = (binary, input_path)
    result = run(cmd, check=True, timeout=30, capture_output=True, text=True)

    # Remove path from output
    actual = result.stdout.rsplit(" ", 1)[0]
    expected = "08403     1"
    assert actual == expected
